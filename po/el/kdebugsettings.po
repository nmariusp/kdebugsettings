# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Petros Vidalis <pvidalis@gmail.com>, 2017, 2018.
# Stelios <sstavra@gmail.com>, 2019, 2020, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-14 01:55+0000\n"
"PO-Revision-Date: 2023-01-06 17:46+0200\n"
"Last-Translator: Stelios <sstavra@gmail.com>\n"
"Language-Team: Greek <kde-i18n-el@kde.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.3\n"

#: src/apps/main.cpp:27 src/quickapps/main.cpp:36
#, kde-format
msgid "KDebugSettings"
msgstr "KdebugSettings"

#: src/apps/main.cpp:29 src/quickapps/main.cpp:38
#, kde-format
msgid "Configure debug settings"
msgstr "Διαμόρφωση ρυθμίσεων αποσφαλμάτωσης"

#: src/apps/main.cpp:31
#, kde-format
msgid "(c) 2015-%1 kdebugsettings authors"
msgstr "(c) 2015-%1 οι συγγραφείς του kdebugsettings"

#: src/apps/main.cpp:32 src/quickapps/main.cpp:41
#, kde-format
msgid "Laurent Montel"
msgstr "Laurent Montel"

#: src/apps/main.cpp:32 src/quickapps/main.cpp:41
#, kde-format
msgid "Maintainer"
msgstr "Συντηρητής"

#: src/apps/main.cpp:38 src/quickapps/main.cpp:47
#, kde-format
msgid ""
"Enable QStandardPaths test mode, i.e. read/write settings used by unittests"
msgstr ""
"Ενεργοποίηση δοκιμαστικής λειτουργίας QStandardPaths, δηλ. ρυθμίσεις "
"ανάγνωσης/εγγραφής σε unittests"

#: src/apps/main.cpp:41 src/quickapps/main.cpp:50
#, kde-format
msgid "Activate full debug for all modules."
msgstr "Να ενεργοποιηθεί η πλήρης αποσφαλμάτωση για όλα τα αρθρώματα."

#: src/apps/main.cpp:43 src/quickapps/main.cpp:52
#, kde-format
msgid "Disable full debug for all modules."
msgstr "Να απενεργοποιηθεί η πλήρης αποσφαλμάτωση για όλα τα αρθρώματα."

#: src/apps/main.cpp:47 src/quickapps/main.cpp:56
#, kde-format
msgid "Change debug mode as console (in console)"
msgstr "Αλλαγή λειτουργίας αποσφαλμάτωσης από την κονσόλα (στην κονσόλα)"

#: src/apps/main.cpp:51 src/quickapps/main.cpp:60
#, kde-format
msgid ""
"Specify logging category name that you want to change debug mode (in console)"
msgstr ""
"Καθορισμός ονόματος κατηγορίας καταγραφής για το οποίο θέλετε να αλλάξετε τη "
"λειτουργία αποσφαλμάτωσης (στην κονσόλα)"

#: src/apps/main.cpp:65 src/apps/main.cpp:75 src/apps/main.cpp:85
#: src/quickapps/main.cpp:74 src/quickapps/main.cpp:84
#: src/quickapps/main.cpp:94
#, kde-format
msgid "Impossible to change debug mode"
msgstr "Αδυναμία αλλαγής της λειτουργίας αποσφαλμάτωσης"

#: src/core/kdebugsettingsutil.cpp:407
#, kde-format
msgid "Full Debug"
msgstr "Πλήρης αποσφαλμάτωση"

#: src/core/kdebugsettingsutil.cpp:410
#, kde-format
msgid "Info"
msgstr "Πληροφορίες"

#: src/core/kdebugsettingsutil.cpp:413
#, kde-format
msgid "Warning"
msgstr "Προειδοποίηση"

#: src/core/kdebugsettingsutil.cpp:416
#, kde-format
msgid "Critical"
msgstr "Κρίσιμη"

#: src/core/kdebugsettingsutil.cpp:419
#, kde-format
msgid "Off"
msgstr "Ανενεργό"

#: src/core/kdebugsettingsutil.cpp:422 src/core/loggingcategory.cpp:144
#, kde-format
msgid "Undefined"
msgstr "Αόριστο"

#: src/core/loggingcategory.cpp:113
#, kde-format
msgid "Category name: %1"
msgstr "Όνομα κατηγορίας: %1"

#: src/core/loggingcategory.cpp:115
#, kde-format
msgid "Identifier: %1"
msgstr "Αναγνωριστικό: %1"

#: src/core/loggingcategory.cpp:117
#, kde-format
msgid "Default Severity: %1"
msgstr "Προκαθορισμένη αυστηρότητα: %1"

#: src/core/loggingcategory.cpp:126
#, kde-format
msgid "All Debug Messages"
msgstr "Όλα τα μηνύματα αποσφαλμάτωσης"

#: src/core/loggingcategory.cpp:129
#, kde-format
msgid "Info Messages"
msgstr "Μηνύματα πληροφοριών"

#: src/core/loggingcategory.cpp:132
#, kde-format
msgid "Warning Messages"
msgstr "Προειδοποιητικά μηνύματα"

#: src/core/loggingcategory.cpp:135
#, kde-format
msgid "Debug Messages"
msgstr "Μηνύματα αποσφαλμάτωσης"

#: src/core/loggingcategory.cpp:138
#, kde-format
msgid "Critical Messages"
msgstr "Κρίσιμα μηνύματα"

#: src/core/loggingcategory.cpp:141
#, kde-format
msgid "No Debug Messages"
msgstr "Χωρίς μηνύματα αποσφαλμάτωσης"

#: src/quickapps/main.cpp:40
#, fuzzy, kde-format
#| msgid "(c) 2015-%1 kdebugsettings authors"
msgid "(c) %1 kdebugsettings authors"
msgstr "(c) 2015-%1 οι συγγραφείς του kdebugsettings"

#: src/widgets/categorywarning.cpp:16
#, kde-format
msgid ""
"You have a rule as '*=true' or '*=false' which will override all your "
"specific rule. Better to remove it."
msgstr ""
"Έχετε θέσει ένα κανόνα σαν *=true' ή '*=false' το οποίο θα παρακάμψει όλους "
"τους υπόλοιπους κανόνες. Καλύτερα αφαιρέστε το."

#: src/widgets/configurecustomsettingdialog.cpp:21
#, kde-format
msgctxt "@title:window"
msgid "Add custom rule"
msgstr "Προσθήκη προσαρμοσμένου κανόνα"

#: src/widgets/configurecustomsettingdialog.cpp:40
#, kde-format
msgctxt "@title:window"
msgid "Edit custom rule"
msgstr "Επεξεργασία προσαρμοσμένου κανόνα"

#: src/widgets/configurecustomsettingwidget.cpp:21
#, kde-format
msgid "Enable"
msgstr "Ενεργοποίηση"

#: src/widgets/configurecustomsettingwidget.cpp:26
#, kde-format
msgid "Category:"
msgstr "Κατηγορία:"

#: src/widgets/configurecustomsettingwidget.cpp:37
#, kde-format
msgid "Type:"
msgstr "Τύπος:"

#: src/widgets/customdebuglistview.cpp:42
#, kde-format
msgid "Add Rule..."
msgstr "Προσθήκη κανόνα..."

#: src/widgets/customdebuglistview.cpp:44
#, kde-format
msgid "Edit Rule"
msgstr "Επεξεργασία κανόνα"

#: src/widgets/customdebuglistview.cpp:50
#: src/widgets/customdebuglistview.cpp:88
#, fuzzy, kde-format
#| msgid "Remove Rule"
msgid "Remove Rule"
msgid_plural "Remove Rules"
msgstr[0] "Αφαίρεση κανόνα"
msgstr[1] "Αφαίρεση κανόνα"

#: src/widgets/customdebuglistview.cpp:83
#, kde-format
msgid "Do you want to remove this rule?"
msgid_plural "Do you want to remove these %1 rules?"
msgstr[0] "Επιθυμείτε την αφαίρεση αυτού του κανόνα;"
msgstr[1] "Επιθυμείτε πραγματικά την αφαίρεση του κανόνα \"%1\";"

#: src/widgets/customdebugsettingspage.cpp:21
#, kde-format
msgid "Edit..."
msgstr "Επεξεργασία..."

#: src/widgets/customdebugsettingspage.cpp:22
#, kde-format
msgid "Remove..."
msgstr "Αφαίρεση..."

#: src/widgets/customdebugsettingspage.cpp:38
#: src/widgets/kdeapplicationdebugsettingpage.cpp:31
#, kde-format
msgid "Search..."
msgstr "Αναζήτηση..."

#: src/widgets/customdebugsettingspage.cpp:46
#, kde-format
msgid "Add..."
msgstr "Προσθήκη..."

#: src/widgets/environmentplaintextedit.cpp:42
#, kde-format
msgid ""
"No rules have been defined in the environment variable \"QT_LOGGING_RULES\"."
msgstr ""
"Δεν έχουν οριστεί κανόνες στη μεταβλητή περιβάλλοντος \"QT_LOGGING_RULES\"."

#: src/widgets/environmentsettingsrulespage.cpp:28
#, kde-format
msgid ""
"These rules cannot be edited with this application. You need to set them in "
"QT_LOGGING_RULES variable directly."
msgstr ""
"Αυτοί οι κανόνες δε μπορούν να αλλαχθούν με αυτή την εφαρμογή. Πρέπει να "
"τους θέσετε απευθείας στη μεταβλητή QT_LOGGING_RULES."

#: src/widgets/environmentsettingsrulespage.cpp:30
#, kde-format
msgid "Current rules:"
msgstr "Τρέχοντες κανόνες:"

#: src/widgets/groupmanagementdialog.cpp:28
#, kde-format
msgctxt "@title:window"
msgid "Manage Group"
msgstr "Διαχείριση ομάδας"

#: src/widgets/groupmanagementwidget.cpp:47
#: src/widgets/groupmanagementwidget.cpp:50
#: src/widgets/groupmanagementwidget.cpp:52
#, fuzzy, kde-format
#| msgid "Load Group"
msgid "Export Group"
msgstr "Φόρτωση ομάδας"

#: src/widgets/groupmanagementwidget.cpp:50
#, kde-format
msgid "Group exported to %1"
msgstr ""

#: src/widgets/groupmanagementwidget.cpp:52
#, fuzzy, kde-format
#| msgid "Impossible to rename group as '%1'"
msgid "Impossible to export group '%2' to '%1'"
msgstr "Αδυναμία μετονομασίας της ομάδας ως '%1'"

#: src/widgets/groupmanagementwidget.cpp:65
#: src/widgets/groupmanagementwidget.cpp:70
#, kde-format
msgid "Rename Group"
msgstr "Μετονομασία ομάδας"

#: src/widgets/groupmanagementwidget.cpp:65
#, kde-format
msgid "New Name:"
msgstr "Νέο όνομα:"

#: src/widgets/groupmanagementwidget.cpp:70
#, fuzzy, kde-format
#| msgid "Impossible to rename group as '%1'"
msgid "Impossible to rename group as '%1'."
msgstr "Αδυναμία μετονομασίας της ομάδας ως '%1'"

#: src/widgets/groupmanagementwidget.cpp:86
#, fuzzy, kde-format
#| msgid "Rename Group"
msgid "Rename Group..."
msgstr "Μετονομασία ομάδας"

#: src/widgets/groupmanagementwidget.cpp:90
#, kde-format
msgid "Export Group..."
msgstr ""

#: src/widgets/groupmanagementwidget.cpp:95
#, kde-format
msgid "Remove Groups"
msgstr "Αφαίρεση ομάδων"

#: src/widgets/groupmanagementwidget.cpp:100
#, kde-format
msgid "Impossible to remove '%1'"
msgstr "Αδυναμία αφαίρεσης του '%1'"

#: src/widgets/groupmanagementwidget.cpp:100
#, kde-format
msgid "Remove Group"
msgstr "Αφαίρεση ομάδας"

#: src/widgets/kdeapplicationdebugsettingpage.cpp:21
#, kde-format
msgid "Enable All Debug"
msgstr "Ενεργοποίηση όλης της αποσφαλμάτωσης"

#: src/widgets/kdeapplicationdebugsettingpage.cpp:22
#, kde-format
msgid "Turn Off Debug"
msgstr "Απενεργοποίηση αποσφαλμάτωσης"

#: src/widgets/kdeapplicationdebugsettingpage.cpp:23
#, kde-format
msgid "Turn Off All Messages"
msgstr "Απενεργοποίηση όλων των μηνυμάτων"

#: src/widgets/kdebugsettingsdialog.cpp:63
#, kde-format
msgid "KDE Application"
msgstr "Εφαρμογή του KDE"

#: src/widgets/kdebugsettingsdialog.cpp:64
#, kde-format
msgid "Custom Rules"
msgstr "Προσαρμοσμένο κανόνες"

#: src/widgets/kdebugsettingsdialog.cpp:65
#, kde-format
msgid "Rules Settings With Environment Variable"
msgstr "Ρυθμίσεις κανόνων με Μεταβλητή Περιβάλλοντος"

#: src/widgets/kdebugsettingsdialog.cpp:74 src/widgets/savetoolbutton.cpp:15
#, kde-format
msgid "Save As..."
msgstr "Αποθήκευση ως..."

#: src/widgets/kdebugsettingsdialog.cpp:87
#, kde-format
msgid "Insert..."
msgstr "Εισαγωγή..."

#: src/widgets/kdebugsettingsdialog.cpp:153
#, kde-format
msgid "'%1' cannot be opened. Please verify it."
msgstr "'%1' δεν μπορεί να ανοιχτεί.Παρακαλώ επαληθεύστε το."

#: src/widgets/kdebugsettingsdialog.cpp:179
#: src/widgets/kdebugsettingsdialog.cpp:193
#, kde-format
msgid "Insert Categories"
msgstr "Εισαγωγή Κατηγοριών"

#: src/widgets/kdebugsettingsdialog.cpp:179
#, kde-format
msgid "Categories Files"
msgstr "Αρχεία Κατηγοριών"

#: src/widgets/kdebugsettingsdialog.cpp:193
#, kde-format
msgid "Categories from file '%1' inserted."
msgstr "Οι κατηγορίες από το αρχείο '%1' εισήχθησαν."

#: src/widgets/kdebugsettingsdialog.cpp:201
#, kde-format
msgid "Group Name"
msgstr "Όνομα ομάδας"

#: src/widgets/kdebugsettingsdialog.cpp:201
#, kde-format
msgid "Name:"
msgstr "Όνομα:"

#: src/widgets/kdebugsettingsdialog.cpp:206
#, fuzzy, kde-format
#| msgid ""
#| "'%1' is already used as a group name.\n"
#| "Please save as another name."
msgid ""
"<b>'%1'</b> is already used as a group name.\n"
"Please save as another name."
msgstr ""
"Το '%1' χρησιμοποιείται ήδη ως όνομα ομάδας.\n"
"Αποθηκεύστε με άλλο όνομα."

#: src/widgets/kdebugsettingsdialog.cpp:215
#, kde-format
msgid "Can not save as empty name. Please use a new one."
msgstr "Αδυναμία αποθήκευσης με κενό όνομα. Χρησιμοποιήστε ένα νέο."

#: src/widgets/kdebugsettingsdialog.cpp:222
#, kde-format
msgid "Save As"
msgstr "Αποθήκευση ως"

#: src/widgets/kdebugsettingsdialog.cpp:222
#: src/widgets/kdebugsettingsdialog.cpp:230
#, kde-format
msgid "KDebugSettings File (*.kdebugsettingsrules)"
msgstr "Αρχείο KDebugSettings (*.kdebugsettingsrules)"

#: src/widgets/kdebugsettingsdialog.cpp:230
#, kde-format
msgid "Load Debug Settings Files"
msgstr "Φόρτωση αρχείων ρύθμισης αποσφαλμάτωσης"

#: src/widgets/loadgroupmenu.cpp:19
#, kde-format
msgid "Load Group"
msgstr "Φόρτωση ομάδας"

#: src/widgets/loadgroupmenu.cpp:68
#, kde-format
msgid "Manage Group"
msgstr "Διαχείριση ομάδας"

#: src/widgets/loadtoolbutton.cpp:17
#, kde-format
msgid "Load..."
msgstr "Φόρτωση..."

#: src/widgets/loadtoolbutton.cpp:21
#, kde-format
msgid "Load From File..."
msgstr "Φόρτωση από αρχείο..."

#: src/widgets/savetoolbutton.cpp:20
#, kde-format
msgid "Save As File..."
msgstr "Αποθήκευση ως αρχείο..."

#: src/widgets/savetoolbutton.cpp:23
#, kde-format
msgid "Save As Group..."
msgstr "Αποθήκευση ως ομάδα..."

#~ msgid "Rules:"
#~ msgstr "Κανόνες:"

#~ msgid "Rename Groups"
#~ msgstr "Μετονομασία ομάδων"

#~ msgid "All"
#~ msgstr "Όλα"
